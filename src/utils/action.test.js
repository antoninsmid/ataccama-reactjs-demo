// @flow
import React from "react";
import action from "./action";

describe("action() utility", () => {
  it("creates a FSA", () => {
    let expected = {
      type: "ACTION",
      payload: null,
      error: false,
      meta: null
    };
    expect(action("ACTION")).toEqual(expected);
    expected.payload = new Error("Error!");
    expected.error = true;
    expect(action("ACTION", new Error("Error!"))).toEqual(expected);
    expected.meta = { debounce: true };
    expect(action("ACTION", new Error("Error!"), { debounce: true })).toEqual(
      expected
    );
    expect(() => action(4324)).toThrow(
      "Expects at least one argument of type string"
    );
  });
});
