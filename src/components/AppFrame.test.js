// @flow
import React from "react";
import { shallow } from "enzyme";
import AppFrame from "./AppFrame";

describe("<AppFrame />", () => {
  it("renders without crashing", () => {
    const wrapper = shallow(<AppFrame />);
    expect(wrapper.find(AppFrame)).toBeTruthy();
  });
  it("renders an '.AppFrame'", () => {
    const wrapper = shallow(<AppFrame />);
    expect(wrapper.find(".AppFrame")).toHaveLength(1);
  });
  it("renders children", () => {
    const wrapper = shallow(
      <AppFrame>
        <div className="testChild" />
        <div className="testChild" />
      </AppFrame>
    );
    expect(wrapper.find(".testChild")).toHaveLength(2);
    expect(wrapper.contains(<div className="testChild" />)).toBeTruthy();
  });
});
