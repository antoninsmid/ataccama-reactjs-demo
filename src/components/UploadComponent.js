// @flow
import React from "react";
import type { UploadComponentProps } from "containers/types";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Typography, Grid } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  input: {
    display: "none"
  },
  wrongFormat: {
    margin: 8
  }
}));

const UploadComponent = (props: UploadComponentProps) => {
  const classes = useStyles();
  return (
    <Grid container justify="flex-start" className="uploadButtonWrap">
      <input
        accept={props.accept}
        className={classes.input}
        type="file"
        onChange={props.onChange}
        id="raised-button-file"
      />
      <label htmlFor="raised-button-file">
        <Button component="span" color="primary" className="uploadButton">
          Upload json file
        </Button>
      </label>
      {props.wrongFormat && (
        <div className={classes.wrongFormat}>
          <Typography variant="caption" color="secondary" id="wrongFormatText">
            Wrong file format.
          </Typography>
        </div>
      )}
    </Grid>
  );
};

export default UploadComponent;
