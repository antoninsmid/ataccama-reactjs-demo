// @flow
import React from "react";
import type { TableProps } from "containers/types";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  table: {
    width: "100%",
    borderCollapse: "collapse"
  },
  tableWrap: {
    padding: `16px 0px 8px 24px`,
    width: "100%"
  },
  headLine: {
    borderBottom: "solid 1px #d6d9ec",
    borderTop: "solid 1px #d6d9ec",
    background: "#f3f3f7"
  },
  headLineCell: {
    lineHeight: "2.6em",
    paddingLeft: theme.spacing(1),
    fontSize: "0.75rem"
  }
}));

/**
 * Creates the table frame and header
 * @param {*} props
 */
const TableView = (props: TableProps) => {
  const { name, data } = props;
  const classes = useStyles();
  return (
    <div className={`innerTableWrap ${classes.tableWrap}`}>
      <Typography variant="overline" className="title">
        {name}
      </Typography>
      <table className={classes.table}>
        <thead>
          <tr>
            <td className={classes.headLine} />
            {data.map(p => (
              <td key={p} className={classes.headLine}>
                <Typography
                  variant="subtitle2"
                  className={`columnHeader ${classes.headLineCell}`}
                >
                  {p}
                </Typography>
              </td>
            ))}
            <td className={classes.headLine} />
          </tr>
        </thead>
        <tbody>{props.children}</tbody>
      </table>
    </div>
  );
};

export default TableView;
