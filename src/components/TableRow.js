// @flow
import React, { Fragment } from "react";
import type { RowProps } from "containers/types";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, IconButton } from "@material-ui/core";
import {
  ChevronRight as ChevronRightIcon,
  ExpandMore as ExpandMoreIcon,
  Clear as DeleteIcon
} from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  row: {
    lineHeight: "2.8em"
  },
  cell: {
    paddingLeft: theme.spacing(1),
    borderBottom: "solid 1px #e8e8e8",
    borderTop: "solid 1px #e8e8e8"
  },
  evenCell: {
    background: "#f6f7f9"
  },
  clickableCell: {
    cursor: "pointer"
  },
  icon: {
    position: "relative",
    top: "7px"
  },
  deleteButton: {
    margin: 0,
    padding: theme.spacing(1)
  },
  removeCell: {
    width: 49
  }
}));

/**
 * Creates the row with values itself.
 * @param {*} props
 */
const TableRow = (props: RowProps) => {
  const classes = useStyles();
  const { data, showArrow, isOpen, toggleOpen, removeNode } = props;

  // node has no data => root, render directly children
  if (!data) {
    return <Fragment>{props.children}</Fragment>;
  }

  const properties = Object.keys(data);

  return (
    <Fragment>
      <tr className={`row ${classes.row}`}>
        {showArrow ? (
          <td
            className={`arrow ${classes.cell} ${classes.clickableCell}`}
            onClick={toggleOpen}
          >
            {isOpen ? (
              <ExpandMoreIcon className={classes.icon} />
            ) : (
              <ChevronRightIcon className={classes.icon} />
            )}
          </td>
        ) : (
          <td className={classes.cell} />
        )}
        {properties.map(property => (
          <td
            key={property}
            onClick={toggleOpen}
            className={`property ${classes.cell} ${
              showArrow ? classes.clickableCell : ""
            }`}
          >
            <Typography variant="body2">{data[property]}</Typography>
          </td>
        ))}
        <td
          className={`action ${classes.cell} ${classes.clickableCell} ${
            classes.removeCell
          }`}
          align="right"
        >
          <IconButton
            aria-label="Delete"
            className={`delete ${classes.deleteButton}`}
            onClick={removeNode}
          >
            <DeleteIcon />
          </IconButton>
        </td>
      </tr>
      {showArrow && isOpen && (
        <tr className="innerTable">
          <td colSpan={properties.length + 2}>{props.children}</td>
        </tr>
      )}
    </Fragment>
  );
};

export default TableRow;
