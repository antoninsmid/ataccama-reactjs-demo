// @flow
import React from "react";
import { shallow } from "enzyme";
import TableView from "./TableView";

const testData = ["id", "test"];

describe("<TableView />", () => {
  it("renders without crashing", () => {
    const wrapper = shallow(<TableView name="testTable" data={testData} />);
    expect(wrapper.find(TableView)).toBeTruthy();
  });
  it("renders inner table", () => {
    const wrapper = shallow(
      <TableView data={testData} name="testTable">
        <div className="testChild" />
      </TableView>
    );
    expect(wrapper.find(".innerTableWrap")).toHaveLength(1);
    expect(wrapper.find(".testChild")).toHaveLength(1);
    expect(wrapper.find(".title").text()).toContain("testTable");
    expect(wrapper.find(".columnHeader")).toHaveLength(2);
  });
});
