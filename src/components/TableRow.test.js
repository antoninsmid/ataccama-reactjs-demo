// @flow
import React from "react";
import { shallow } from "enzyme";
import sinon from "sinon";
import TableRow from "./TableRow";

describe("<TableRow />", () => {
  it("renders without crashing", () => {
    const wrapper = shallow(<TableRow />);
    expect(wrapper.find(TableRow)).toBeTruthy();
  });
  it("renders directly children if root (no data)", () => {
    const wrapper = shallow(
      <TableRow>
        <div className="testChild" />
      </TableRow>
    );
    expect(wrapper.find(".testChild")).toHaveLength(1);
    expect(wrapper.find(".row")).toHaveLength(0);
    expect(wrapper.find(".action")).toHaveLength(0);
    expect(wrapper.find(".arrow")).toHaveLength(0);
  });
  it("renders the row and children if not root", () => {
    const wrapper = shallow(
      <TableRow data={{ id: 1, name: "test" }} showArrow>
        <div className="testChild" />
      </TableRow>
    );
    expect(wrapper.find(".property")).toHaveLength(2);
    expect(wrapper.find(".testChild")).toHaveLength(0);
    expect(wrapper.find(".row")).toHaveLength(1);
    expect(wrapper.find(".action")).toHaveLength(1);
    expect(wrapper.find(".arrow")).toHaveLength(1);
    expect(wrapper.find(".innerTable")).toHaveLength(0);
  });
  it("renders inner table if opened", () => {
    const wrapper = shallow(
      <TableRow data={{ id: 1, name: "test" }} showArrow isOpen>
        <div className="testChild" />
      </TableRow>
    );
    expect(wrapper.find(".testChild")).toHaveLength(1);
    expect(wrapper.find(".innerTable")).toHaveLength(1);
  });
  it("simulates click on arrow", () => {
    const onButtonClick = sinon.spy();
    const wrapper = shallow(
      <TableRow
        toggleOpen={onButtonClick}
        data={{ id: 1, name: "test" }}
        showArrow
      />
    );
    wrapper.find(".arrow").simulate("click");
    expect(onButtonClick).toHaveProperty("callCount", 1);
  });
  it("simulates click on delete button", () => {
    const onButtonClick = sinon.spy();
    const wrapper = shallow(
      <TableRow
        removeNode={onButtonClick}
        data={{ id: 1, name: "test" }}
        showArrow
      />
    );
    wrapper.find(".delete").simulate("click");
    expect(onButtonClick).toHaveProperty("callCount", 1);
  });
});
