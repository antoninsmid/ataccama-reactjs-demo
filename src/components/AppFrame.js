// @flow
import React from "react";
import type { AppFrameProps } from "containers/types";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Paper } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";

const useStyles = makeStyles(theme => ({
  appContainer: {
    background: theme.palette.background.default,
    marginTop: theme.spacing(4)
  },
  tableWrap: {
    padding: theme.spacing(4),
    paddingLeft: theme.spacing(1)
  }
}));

const AppFrame = (props: AppFrameProps) => {
  const classes = useStyles();
  return (
    <div className="AppFrame">
      <CssBaseline />
      <Grid container className={classes.appContainer} justify="center">
        <Grid item sm={12} md={11} lg={10} xl={9}>
          <Paper elevation={4} className={classes.tableWrap}>
            {props.children}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default AppFrame;
