// @flow
import React from "react";
import { shallow, mount } from "enzyme";
import UploadComponent from "./UploadComponent";

describe("<UploadComponent />", () => {
  it("renders without crashing", () => {
    const wrapper = shallow(<UploadComponent />);
    expect(wrapper.find(UploadComponent)).toBeTruthy();
  });
  it("renders an '.UploadComponent'", () => {
    const wrapper = shallow(<UploadComponent />);
    expect(wrapper.find(".uploadButton")).toHaveLength(1);
    expect(wrapper.find(".uploadButtonWrap")).toHaveLength(1);
    expect(wrapper.find("#raised-button-file")).toHaveLength(1);
    expect(wrapper.find(".uploadButton").text()).toContain("Upload json file");
  });
  it("renders wrong format", () => {
    const wrapper = shallow(<UploadComponent wrongFormat />);
    expect(wrapper.find("#wrongFormatText").text()).toContain(
      "Wrong file format."
    );
  });
  // test load input file
});
