// @flow
import { combineReducers } from "redux";

// reducer for table visualizing app
import tableApp from "containers/App/duck";

// reducers here
export default combineReducers({
  tableApp
});
