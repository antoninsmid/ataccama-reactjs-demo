// @flow
import * as React from "react";

export type Record = {
  id: number,
  data?: any,
  kids: Array<number>,
  kidsName: string,
  kidsHeader: Array<string>,
  parentId: number
};

export type Store = {
  records: Map<number, Record>,
  opened: Map<number, boolean>
};

export type RecordProps = {
  ...Store,
  id: number,
  removeNode: number => void,
  toggleOpen: number => void,
  record: Record,
  opened: boolean,
  index: number
};

export type AppProps = Store & {
  recordsEmpty: boolean,
  setTableData: Record => void
};

export type FileUpload = {
  setTableData: any => void
};

// component properties

export type RowProps = {
  data: any,
  showArrow: boolean,
  isOpen: boolean,
  toggleOpen: () => void,
  removeNode: () => void,
  children?: React.Node
};

export type TableProps = {
  name: string,
  data: Array<string>,
  children?: React.Node
};

export type AppFrameProps = {
  children?: React.Node
};

export type UploadComponentProps = {
  accept: string,
  onChange: any => void,
  wrongFormat: boolean
};
