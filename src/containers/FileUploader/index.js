// @flow
import React, { useState } from "react";
import type { FileUpload, Record } from "../types";
import { Map } from "immutable";
import { ROOT_ID, ROOT_PARENT_ID } from "containers/App/duck";
import UploadComponent from "components/UploadComponent";

/**
 * Initializes the tree table as closed
 * @param {*} records
 */
export const initOpened = (
  records: Map<number, Record>
): Map<number, boolean> => {
  let opened = new Map();
  records.forEach((value, key) => {
    opened = opened.set(key, false);
  });
  return opened;
};

/**
 * BFS processing of the nodes, flatten the structure to be used with Redux
 * https://redux.js.org/recipes/structuring-reducers/normalizing-state-shape
 *
 * @param {hierarchical structure from JSON} recordsRaw
 * @param {destination map holding ids and records} records
 * @param {id of the parent node} parentId
 */
export const processNodes = (
  recordsRaw: Array<any>,
  records: Map<number, Record>,
  parentId: number
): Map<number, Record> => {
  let recordsOut = records;
  recordsRaw.forEach(rec => {
    const id = recordsOut.size;
    const newRecord: Record = {
      // create new node
      data: rec.data,
      id,
      parentId,
      kids: [],
      kidsHeader: [],
      kidsName: ""
    };
    recordsOut = recordsOut.set(id, newRecord);

    // add the id of this node, to parent children array
    if (parentId || parentId === 0) {
      const parent = recordsOut.get(parentId);
      if (parent) {
        parent.kids = [...parent.kids, id];

        // set parent's children table header, if not initialized yet
        if (parent.kidsHeader.length === 0) {
          parent.kidsHeader = Object.keys(rec.data);
        }
      }
    }

    // in case node has children, process them
    if (Object.keys(rec.kids).length > 0) {
      const kidsName = Object.keys(rec.kids)[0];
      newRecord.kidsName = kidsName;
      recordsOut = processNodes(rec.kids[kidsName].records, recordsOut, id);
    }
  });
  return recordsOut;
};

/**
 * Handles the file processing
 *
 * @param {original loaded file to use for meatada} file
 * @param {file contents as utf-8 string} fileAsText
 * @param {method to save the result into Redux} setter
 */
export const processUpload = (
  file: any,
  fileAsText: string,
  setter: any => void,
  setWrongFormat: boolean => void
): void => {
  try {
    let records = new Map();
    // create a parent with no data to hold the root array
    records = records.set(ROOT_ID, {
      id: ROOT_ID,
      kids: [],
      parentId: ROOT_PARENT_ID,
      kidsHeader: [],
      kidsName: file[0].name
    });
    // flatten the hierarchy
    records = processNodes(JSON.parse(fileAsText), records, ROOT_ID);
    // initialize the log for which node is opened
    const opened: Map<number, boolean> = initOpened(records);
    // save data to Redux
    setter({ records, opened });
  } catch (e) {
    console.error("Wrong format.");
    setWrongFormat(true);
  }
};

/**
 * Convert the uploaded utf8 file to string and invokes processing method
 * @param {raw uploaded file} file
 * @param {the method to call with text parameter} callback
 * @param {method to be called later to persist data into Redux} dataSetter
 */
const fileToText = (
  file: any,
  callback: (
    file: any,
    fileAsText: string,
    setter: (any) => void,
    setWrongFormat: (boolean) => void
  ) => void,
  dataSetter: any => void,
  setWrongFormat: boolean => void
): void => {
  if (!file[0]) {
    return;
  }
  const reader = new FileReader();
  reader.readAsText(file[0]);
  reader.onload = () => {
    callback(file, reader.result, dataSetter, setWrongFormat);
  };
};

const FileUploader = (props: FileUpload) => {
  const { setTableData } = props;
  const [isWrongFormat, setWrongFormat] = useState(false);
  return (
    <UploadComponent
      accept=".json"
      onChange={event =>
        fileToText(
          event.target.files,
          processUpload,
          setTableData,
          setWrongFormat
        )
      }
      wrongFormat={isWrongFormat}
    />
  );
};

export default FileUploader;
