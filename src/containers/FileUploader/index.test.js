// @flow
import React from "react";
import { initOpened, processNodes } from "./index";
import { Map } from "immutable";
import { ROOT_ID } from "../App/duck";

const recordsRaw = [
  {
    data: {
      name: "test1"
    },
    kids: {
      has_relatives: {
        records: [
          {
            data: {
              surname: "relativeName1"
            },
            kids: {
              has_phone: {
                records: [
                  {
                    data: {
                      phone: "phoneName1"
                    },
                    kids: {}
                  }
                ]
              }
            }
          }
        ]
      }
    }
  }
];

export const getRecords = () => {
  let records = new Map();
  records = records.set(0, {
    id: 0,
    kids: [1],
    parentId: ROOT_ID,
    kidsHeader: ["surname"],
    kidsName: "has_relatives",
    data: { name: "test1" }
  });
  records = records.set(1, {
    id: 1,
    kids: [2],
    parentId: 0,
    kidsHeader: ["phone"],
    kidsName: "has_phone",
    data: { surname: "relativeName1" }
  });
  records = records.set(2, {
    id: 2,
    kids: [],
    parentId: 1,
    kidsHeader: [],
    kidsName: "",
    data: { phone: "phoneName1" }
  });
  return records;
};

export const getOpened = () => {
  let opened = new Map();
  return opened
    .set(0, false)
    .set(1, false)
    .set(2, false);
};

describe("FileUploaderFunctions", () => {
  it("initialize default opened", () => {
    const opened = initOpened(getRecords());
    expect(opened).toEqual(getOpened());
  });
  it("process Nodes", () => {
    const processedRecords = processNodes(recordsRaw, new Map(), ROOT_ID);
    expect(processedRecords).toEqual(getRecords());
  });
});
