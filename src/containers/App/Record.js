// @flow
import React from "react";
import type { RecordProps } from "containers/types";
import TableRow from "components/TableRow";
import TableView from "../../components/TableView";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as selectors from "./selectors";
import { removeNode, toggleOpen } from "./duck";

/**
 * Represents a single record - one node in the hierarchy
 * Contains both the data and information about children
 *
 * @param {*} props
 */
class _Record extends React.PureComponent<RecordProps> {
  toggleOpen = () => {
    this.props.toggleOpen(this.props.id);
  };

  removeNode = () => {
    this.props.removeNode(this.props.id);
  };

  render() {
    const { record, opened, removeNode, toggleOpen } = this.props;

    const isLeaf = record.kids.length === 0;
    return (
      <TableRow
        className="tableRow"
        data={record.data}
        showArrow={!isLeaf}
        isOpen={opened}
        toggleOpen={this.toggleOpen}
        removeNode={this.removeNode}
      >
        {!isLeaf && (
          <TableView
            name={record.kidsName}
            data={record.kidsHeader || []}
            className="tableView"
          >
            {record.kids.map((r, index) => (
              <Record
                key={r}
                id={r}
                removeNode={removeNode}
                toggleOpen={toggleOpen}
                opened={opened}
              />
            ))}
          </TableView>
        )}
      </TableRow>
    );
  }
}

const mapDispatch = dispatch => {
  return bindActionCreators({ removeNode, toggleOpen }, dispatch);
};

const mapState = () => {
  const getRecordAndOpen = selectors.makeGetRecordAndOpen();
  return (state, ownProps) => {
    return { ...getRecordAndOpen(state, ownProps) };
  };
};

const Record = connect(
  mapState,
  mapDispatch
)(_Record);

export default Record;
