/**
 * Test action creators together with reducer
 *
 * @flow
 */
import reducer, * as other from "./duck";
import { getRecords, getOpened } from "../FileUploader/index.test";
import { Map } from "immutable";

describe("table app reducer", () => {
  it("return initial state", () => {
    const store = reducer(undefined, {});
    expect(store).toEqual(other.defaultStore);
  });
  it("sets the data", () => {
    const expectedStore = { records: getRecords(), opened: getOpened() };
    let currentStore = reducer(undefined, other.setTableData(expectedStore));
    expect(currentStore).toEqual(expectedStore);
  });
  it("sets open/close tab", () => {
    let expectedStore = { records: getRecords(), opened: getOpened() };
    // set 0 to opened
    let currentStore = reducer(expectedStore, other.toggleOpen(0));
    expectedStore = {
      ...expectedStore,
      opened: expectedStore.opened.set(0, true)
    };
    expect(currentStore).toEqual(expectedStore);
    // set 0 back to closed
    currentStore = reducer(expectedStore, other.toggleOpen(0));
    expectedStore = {
      ...expectedStore,
      opened: expectedStore.opened.set(0, false)
    };
    expect(currentStore).toEqual(expectedStore);
  });
  it("removes a leaf node", () => {
    let expectedStore = { records: getRecords(), opened: getOpened() };
    let currentStore = reducer(expectedStore, other.removeNode(2));
    let newRecords = new Map();
    newRecords = newRecords.set(0, expectedStore.records.get(0));
    const singleRecord = expectedStore.records.get(1);
    singleRecord.kids = [];
    newRecords = newRecords.set(1, singleRecord);
    let newOpened = new Map();
    newOpened = newOpened.set(0, false).set(1, false);
    expectedStore = {
      records: newRecords,
      opened: newOpened
    };
    expect(currentStore).toEqual(expectedStore);
  });
  it("removes node with children", () => {
    let expectedStore = { records: getRecords(), opened: getOpened() };
    let currentStore = reducer(expectedStore, other.removeNode(1));
    let newRecords = new Map();
    const singleRecord = expectedStore.records.get(0);
    singleRecord.kids = [];
    newRecords = newRecords.set(0, singleRecord);
    let newOpened = new Map();
    newOpened = newOpened.set(0, expectedStore.opened.get(0));
    expectedStore = {
      records: newRecords,
      opened: newOpened
    };
    expect(currentStore).toEqual(expectedStore);
  });
});
