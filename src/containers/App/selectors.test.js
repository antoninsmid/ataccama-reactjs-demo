//@flow
import { defaultStore } from "./duck";
import * as s from "./selectors";
import configureStore from "redux-mock-store";
import { Map } from "immutable";

describe("Table selectors", () => {
  it("return empty if not initialized", () => {
    const mockStore = configureStore();
    const store = mockStore({ tableApp: defaultStore });
    const isEmpty = s.recordsEmpty(store.getState());
    expect(isEmpty).toEqual(true);
  });
  it("return not empty if initialized", () => {
    const mockStore = configureStore();
    let records = new Map();
    records = records.set(1, { id: 1 });
    let opened = new Map();
    opened = opened.set(1, true);
    const store = mockStore({ tableApp: { records, opened } });
    const isEmpty = s.recordsEmpty(store.getState());
    expect(isEmpty).toEqual(false);
  });
  it("return records from store", () => {
    const mockStore = configureStore();
    let records = new Map();
    records = records.set(1, { id: 1 });
    records = records.set(2, { id: 2 });
    let opened = new Map();
    opened = opened.set(1, true);
    opened = opened.set(2, false);
    const store = mockStore({ tableApp: { records, opened } });
    const getRecordAndOpen = s.makeGetRecordAndOpen();
    const ro1 = getRecordAndOpen(store.getState(), { id: 1 });
    expect(ro1).toEqual({ record: { id: 1 }, opened: true });
    const ro2 = getRecordAndOpen(store.getState(), { id: 2 });
    expect(ro2).toEqual({ record: { id: 2 }, opened: false });
  });
});
