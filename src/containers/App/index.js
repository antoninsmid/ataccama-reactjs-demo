// @flow
import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as selectors from "./selectors";
import type { AppProps } from "../types";
import { setTableData, ROOT_ID } from "./duck";
import FileUploader from "containers/FileUploader";
import Record from "./Record";
import AppFrame from "components/AppFrame";

const App = (props: AppProps) => {
  return (
    <AppFrame>
      {!props.recordsEmpty ? (
        <Record {...props} id={ROOT_ID} /> //the root node has id -1
      ) : (
        <FileUploader setTableData={props.setTableData} />
      )}
    </AppFrame>
  );
};

const mapDispatch = dispatch => {
  return bindActionCreators({ setTableData }, dispatch);
};

const mapState = state => {
  return { recordsEmpty: selectors.recordsEmpty(state) };
};

export default connect(
  mapState,
  mapDispatch
)(App);
