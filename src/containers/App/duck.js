/*
 * Action creators and reducers in duck
 * following this pattern https://github.com/erikras/ducks-modular-redux
 *
 * @flow
 **/
import action from "utils/action";
import type { Action } from "utils/action";
import type { Store, Record } from "../types";

export const ROOT_ID = -1;
export const ROOT_PARENT_ID = -2;

export const Actions = {
  SET_TABLE_DATA: "ataccama/tableApp/SET_TABLE_DATA",
  REMOVE_NODE: "ataccama/tableApp/REMOVE_NODE",
  TOGGLE_OPEN: "ataccama/tableApp/TOGGLE_OPEN"
};

export const defaultStore: Store = {
  records: new Map(),
  opened: new Map()
};

/**
 * Handle the node removal
 * @param {*id of the node to remove} id
 * @param {*} state
 */
const findAndRemoveNode = (id: number, state: Store): Store => {
  let newState: Store = { ...state };
  const record: ?Record = newState.records.get(id);

  // remove kids if some
  if (record && record.kids.length > 0) {
    record.kids.forEach(kid => {
      newState = findAndRemoveNode(kid, state);
    });
  }

  // remove from parent's kidsArray if not at root level
  const pid = record.parentId;
  if (pid !== ROOT_PARENT_ID) {
    const parent = { ...state.records.get(pid) };
    parent.kids = parent.kids.filter(k => k !== id);
    newState = {
      ...newState,
      records: newState.records.set(pid, parent)
    };
  }

  // finally remove self
  newState = {
    records: newState.records.delete(id),
    opened: newState.opened.delete(id)
  };

  return newState;
};

/**
 * Handle open and close the nested structure
 * @param {*id of the node} id
 * @param {*} state
 * @param {*is going to be opened} desiredState
 */
const processOpenedChange = (
  id: number,
  state: Store,
  desiredState: boolean
): Store => {
  // open
  if (desiredState) {
    return {
      ...state,
      opened: state.opened.set(id, true)
    };
  }
  //close including chindren
  let newState = {
    ...state,
    opened: state.opened.set(id, false)
  };
  state.records.get(id).kids.forEach(kid => {
    newState = processOpenedChange(kid, newState, false);
  });
  return newState;
};

// Reducer
export default function reducer(
  state: Store = defaultStore,
  action: Action
): Store {
  switch (action.type) {
    case Actions.SET_TABLE_DATA:
      return {
        ...action.payload
      };
    case Actions.REMOVE_NODE:
      return {
        ...findAndRemoveNode(action.payload, state)
      };
    case Actions.TOGGLE_OPEN:
      return {
        ...processOpenedChange(
          action.payload,
          state,
          !state.opened.get(action.payload)
        )
      };
    default:
      return state;
  }
}

// Action creators
export function toggleOpen(payload: number): Action {
  return action(Actions.TOGGLE_OPEN, payload);
}
export function setTableData(payload: Store): Action {
  return action(Actions.SET_TABLE_DATA, payload);
}
export function removeNode(payload: Array<number>): Action {
  return action(Actions.REMOVE_NODE, payload);
}
