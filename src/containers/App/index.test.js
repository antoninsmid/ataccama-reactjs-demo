// @flow
import React from "react";
import { shallow, mount } from "enzyme";
import configureStore from "redux-mock-store";
import { defaultStore, ROOT_ID } from "./duck";
import App from "./index";
import FileUploader from "../FileUploader/index";
import Record from "./Record";
import AppFrame from "../../components/AppFrame";

describe("AppFrame", () => {
  it("connects to Redux", () => {
    const mockStore = configureStore();
    const store = mockStore({ tableApp: defaultStore });
    const wrapper = shallow(<App store={store} />);
    const appFrameProps = wrapper.props().children.props;
    expect(appFrameProps.setTableData).toBeTruthy();
    expect(appFrameProps.recordsEmpty).toEqual(true);
  });
  it("renders file uploader", () => {
    const mockStore = configureStore();
    const store = mockStore({ tableApp: defaultStore });
    const wrapper = mount(<App store={store} />);
    expect(wrapper.find(FileUploader)).toHaveLength(1);
    expect(wrapper.find(Record)).toHaveLength(0);
    expect(wrapper.find(".AppFrame")).toHaveLength(1);
  });
  it("renders Record", () => {
    const mockStore = configureStore();
    let records = new Map();
    records = records.set(ROOT_ID, {
      id: 1,
      kids: [],
      kidsName: "test",
      parentId: 0
    });
    let opened = new Map();
    opened = opened.set(1, true);
    const store = mockStore({ tableApp: { records, opened } });
    const wrapper = mount(<App store={store} />);
    expect(wrapper.find(FileUploader)).toHaveLength(0);
    expect(wrapper.find(Record)).toHaveLength(1);
    expect(wrapper.find(".AppFrame")).toHaveLength(1);
  });
});
