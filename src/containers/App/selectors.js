/**
 * Simple selectors
 */
import { createSelector } from "reselect";

export const recordsEmpty = createSelector(
  state => state.tableApp.records,
  records => {
    return records.size === 0;
  }
);

export const makeGetRecordAndOpen = () =>
  createSelector(
    (state, props) => props.id,
    state => state.tableApp.records,
    state => state.tableApp.opened,
    (nodeId, records, opened) => {
      return {
        record: records.get(nodeId),
        opened: opened.get(nodeId)
      };
    }
  );
