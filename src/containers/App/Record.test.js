// @flow
import React from "react";
import { shallow, mount } from "enzyme";
import configureStore from "redux-mock-store";
import { defaultStore, ROOT_ID } from "./duck";
import Record from "./Record";
import { getRecords, getOpened } from "../FileUploader/index.test";
import { Map } from "immutable";
import { Provider } from "react-redux";

const getOpenedTrue = () => {
  let opened = new Map();
  return opened.set(0, true).set(1, true);
};

describe("Record", () => {
  it("connects to Redux with correct props", () => {
    const mockStore = configureStore();
    const store = mockStore({
      tableApp: { records: getRecords(), opened: getOpened() }
    });
    const wrapper = shallow(<Record store={store} id={0} />);
    const recordProps = wrapper.props().children.props;
    expect(recordProps.removeNode).toBeTruthy();
    expect(recordProps.toggleOpen).toBeTruthy();
    expect(recordProps.record.kidsHeader).toEqual(["surname"]);
    expect(recordProps.opened).toEqual(false);
  });
  it("renders data rows and child tables", () => {
    const mockStore = configureStore();
    const store = mockStore({
      tableApp: { records: getRecords(), opened: getOpenedTrue() }
    });
    const wrapper = mount(
      <Provider store={store}>
        <table>
          <tbody>
            <Record id={0} />
          </tbody>
        </table>
      </Provider>
    );
    expect(wrapper.find(".tableRow")).toHaveLength(3);
    expect(wrapper.find(".tableView")).toHaveLength(2);
  });
});
