# Ataccama ReactJS test task

## Run on localhost

```
cd ataccama-reactjs-demo
npm test
npm start
```

Then, load the JSON file you want to test.

## Features

- Render JSON file as a table in material design
- UI and Redux tests

## Technologies

- [React (UI)](https://reactjs.org/)
- [Redux (state management)](https://redux.js.org/)
- [Enzyme (UI tests for React)](http://airbnb.io/enzyme/)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
